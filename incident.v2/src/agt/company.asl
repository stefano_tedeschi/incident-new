/* Initial goals */

!start.

/* Plans */

+!start
	<- createWorkspace("ora4mas");
       joinWorkspace("ora4mas",WOrg);

       makeArtifact(myorg, "ora4mas.nopl.OrgBoard", ["src/org/incident.xml"],_)[wid(WOrg)];
       
       createGroup(c_am_group,c_am_group,Gr1);
       debug(inspector_gui(on))[artifact_id(Gr1)];
       createGroup(key_account_group,key_account_group,Gr2);
       debug(inspector_gui(on))[artifact_id(Gr2)];
       createGroup(am_fl_group,am_fl_group,Gr3);
       debug(inspector_gui(on))[artifact_id(Gr3)];
       createGroup(first_level_group,first_level_group,Gr4);
       debug(inspector_gui(on))[artifact_id(Gr4)];
       
       .send(c, achieve, join(myorg,c_am_group,customer));
       .send(m1, achieve, join(myorg,c_am_group,key_account_manager));
       
       .send(m1, achieve, join(myorg,key_account_group,key_account_manager));
       .send(w1, achieve, join(myorg,key_account_group,key_account_worker1));
       .send(w2, achieve, join(myorg,key_account_group,key_account_worker2));
       .send(w3, achieve, join(myorg,key_account_group,key_account_worker3));

	   .send(w3, achieve, join(myorg,am_fl_group,key_account_worker3));
	   .send(m2, achieve, join(myorg,am_fl_group,first_level_manager));
	   
	   .send(m2, achieve, join(myorg,first_level_group,first_level_manager));
	   .send(w4, achieve, join(myorg,first_level_group,first_level_worker1));

	   .

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }