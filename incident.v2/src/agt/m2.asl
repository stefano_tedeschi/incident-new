+obligation(Ag,_,What,_)
	 : .my_name(Ag) &
	   done(sch5,first_level_management,Ag)=What &
	   grArt(first_level_group,GrArtId)
	<- createScheme(sch6, scheme6, SchArtId);
       focus(SchArtId);
       addSchemeWhenFormationOk(sch6)[artifact_id(GrArtId)].

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }