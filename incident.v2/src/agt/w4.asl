+obligation(Ag,_,What,_)
	 : .my_name(Ag) &
	   done(sch6,handle_fl_issue,Ag)=What &
	   scheme(sch6,_,SchId)
	<- println("Handling first level issue...");
	   goalAchieved(handle_fl_issue)[artifact_id(SchId)].
	   
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }