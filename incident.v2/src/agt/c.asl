+play(Me,customer,_)
	 : .my_name(Me)
	<- .wait(100);
	   !solve_a_problem.

+!solve_a_problem
	 : orgArt(myorg,OrgArtId) &
	   grArt(c_am_group,GrArtId)
	<- createScheme(sch1, scheme1, SchArtId)[artifact_id(OrgArtId)];
       focus(SchArtId);
       addSchemeWhenFormationOk(sch1)[artifact_id(GrArtId)]; //report-problem
       println("I have a problem!").

+ask_description[source(Sender)]
	<- println("Sending description...");
	   //.send(Sender,tell,description(easyProblem)).
	   .send(Sender,tell,description(hardProblem)).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }