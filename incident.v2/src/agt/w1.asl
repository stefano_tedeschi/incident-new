+obligation(Ag,_,What,_)[artifact_id(ArtId)]
	 : .my_name(Ag) &
	   done(sch2,get_description,Ag)=What
	<- println("Asking for description...");
	   .send(c,tell,ask_description).
	   
+obligation(Ag,_,What,_)[artifact_id(ArtId)]
	 : .my_name(Ag) &
	   done(sch2,get_description,Ag)=What
	<- println("Asking for description...");
	   .send(c,tell,ask_description).

+description(D)
	 : play(Am,key_account_manager,key_account_group)
	<- .send(Am,tell,description(D));
	   .wait(100);
	   goalAchieved(get_description).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }