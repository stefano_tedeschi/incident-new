+obligation(Ag,_,What,_)
	 : .my_name(Ag) &
	   done(sch1,problem_management,Ag)=What &
	   grArt(key_account_group,GrArtId)
	<- println("The customer has a problem!");
	   createScheme(sch2, scheme2, SchArtId);
	   debug(inspector_gui(on))[artifact_id(SchArtId)];
       focus(SchArtId);
       addSchemeWhenFormationOk(sch2)[artifact_id(GrArtId)].
       
+oblUnfulfilled(O)
	 : .my_name(Ag) &
	   obligation(Ag,_,What,_) = O &
	   done(sch1,problem_management,Ag)=What
	<- println("Failure in problem management!").

+goalState(sch2,get_description,_,_,satisfied)
	 : description(easyProblem) &
	   grArt(key_account_group,GrArtId)
	<- createScheme(sch3, scheme3, SchArtId);
       focus(SchArtId);
       addSchemeWhenFormationOk(sch3)[artifact_id(GrArtId)].

+goalState(sch2,get_description,_,_,satisfied)
	 : description(hardProblem)
	<- createScheme(sch4, scheme4, SchArtId);
       focus(SchArtId);
       addSchemeWhenFormationOk(sch4)[artifact_id(GrArtId)].
	   
+goalState(sch3,explain_solution,_,_,satisfied)
	 : scheme(sch1,_,SchId)
	<- println("DONE");
	   goalAchieved(problem_management)[artifact_id(SchId)].

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }