+obligation(Ag,_,What,_)
	 : .my_name(Ag) &
	   done(sch4,ask_fl_support,Ag)=What &
	   scheme(sch4,_,Sch4Id) &
	   grArt(am_fl_group,GrArtId)
	<- println("Asking first level support...");
       createScheme(sch5, scheme5, Sch5ArtId);
       focus(Sch5ArtId);
       addSchemeWhenFormationOk(sch5)[artifact_id(GrArtId)];
	   goalAchieved(ask_fl_support)[artifact_id(Sch4Id)].

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }