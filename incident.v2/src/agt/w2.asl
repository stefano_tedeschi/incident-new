+obligation(Ag,_,What,_)
	 : .my_name(Ag) &
	   done(sch3,explain_solution,Ag)=What &
	   scheme(sch3,_,SchId)
	<- println("Explaining the solution...");
	   goalAchieved(explain_solution)[artifact_id(SchId)].

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("common.asl") }